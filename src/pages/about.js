import * as React from "react";
import { graphql, Link } from "gatsby";

// Local Components
import { ImageContainer } from "../components";

const About = ({
  data: {
    aboutPage: {
      title,
      description,
      headlineImage: { gatsbyImageData, alt },
    },
  },
}) => {
  return (
    <main>
      <h1>{title}</h1>
      <p>{description}</p>
      <ImageContainer image={gatsbyImageData} alt={alt} />
      <Link
        to="/"
        style={{
          marginTop: "3rem",
          display: "block",
        }}
      >
        Back to Home
      </Link>
    </main>
  );
};

export default About;

export const query = graphql`
  query {
    aboutPage: datoCmsAboutPage {
      title
      description
      headlineImage {
        gatsbyImageData(placeholder: BLURRED)
        alt
      }
    }
  }
`;
