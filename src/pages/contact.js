import React from "react";

// data
import { useDatoContactPage } from "../datoQuery";

import { Link } from "gatsby";

const Contact = () => {
  const { title, headline, description } = useDatoContactPage();

  return (
    <div>
      <Title title={title} />
      <Headline headline={headline} />
      <Description description={description} />

      <Link to="/">Back to Home</Link>
    </div>
  );
};

export default Contact;

const Title = ({ title }) => {
  return (
    <h1
      style={{
        fontSize: "1.4rem",
        fontWeight: "bold",
      }}
    >
      {title}
    </h1>
  );
};

const Headline = ({ headline }) => {
  return (
    <h2
      style={{
        fontSize: "3rem",
        fontWeight: "bold",
      }}
    >
      {headline}
    </h2>
  );
};

const Description = ({ description }) => {
  return <p>{description}</p>;
};
