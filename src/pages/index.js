import * as React from "react";
import { graphql, Link } from "gatsby";

const IndexPage = ({
  data: {
    landingPage: { title, description },
    posts,
  },
}) => {
  return (
    <main>
      <h1>{title}</h1>
      <p>{description}</p>
      <LinksContainer posts={posts} />
    </main>
  );
};

export default IndexPage;

export const query = graphql`
  query {
    landingPage: datoCmsLandingPage {
      title
      description
    }
    posts: allDatoCmsPost {
      nodes {
        title
        slugForPost
      }
    }
  }
`;

const LinksContainer = ({ posts }) => {
  return (
    <div
      style={{
        mt: "30px",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Link to="/about">About Page</Link>
      <Link to="/contact">Contact Page</Link>
      <p
        style={{
          fontSize: "1.3rem",
          fontWeight: "bold",
          marginTop: "4rem",
        }}
      >
        POSTS
      </p>
      {posts.nodes.map((post, index) => {
        return <Link to={`/post/${post.slugForPost}`}>{post.title}</Link>;
      })}
    </div>
  );
};
