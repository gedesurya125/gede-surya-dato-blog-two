import React from "react";
import { GatsbyImage } from "gatsby-plugin-image";

export const ImageContainer = ({ image, alt, ...props }) => {
  return (
    <div {...props}>
      <GatsbyImage
        image={image}
        alt={alt}
        style={{
          width: "100%",
        }}
      />
    </div>
  );
};
