import React from "react";

import { graphql, Link } from "gatsby";

const Post = ({
  data: {
    post: { title, headline, description },
  },
}) => {
  return (
    <div>
      <Title title={title} />
      <Headline headline={headline} />
      <Description description={description} />
      <Link
        to="/"
        style={{
          display: "block",
          marginTop: "10rem",
        }}
      >
        Back to home
      </Link>
    </div>
  );
};

export default Post;

const Title = ({ title }) => {
  return <h1 style={{ fontSize: "1.5rem", fontWeight: "bold" }}>{title}</h1>;
};

const Headline = ({ headline }) => {
  return <h2 style={{ fontSize: "2.5rem", fontWeight: "bold" }}>{headline}</h2>;
};
const Description = ({ description }) => {
  return <p style={{ fontSize: "1rem" }}>{description}</p>;
};

export const query = graphql`
  query MyQuery($slug: String) {
    post: datoCmsPost(slugForPost: { eq: $slug }) {
      headline
      title
      description
      content {
        blocks
        links
        value
      }
    }
  }
`;
