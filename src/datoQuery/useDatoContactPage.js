import { useStaticQuery, graphql } from "gatsby";

export const useDatoContactPage = () => {
  const data = useStaticQuery(graphql`
    query {
      data: datoCmsContactPage {
        title
        headline
        description
      }
    }
  `);
  return data.data;
};
