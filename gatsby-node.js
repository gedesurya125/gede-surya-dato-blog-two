exports.createPages = async function ({ actions, graphql }) {
  const { data } = await graphql(`
    query {
      allDatoCmsPost {
        nodes {
          id
          slugForPost
        }
      }
    }
  `);
  data.allDatoCmsPost.nodes.forEach((node) => {
    const slug = node.slugForPost;
    actions.createPage({
      path: `/post/${slug}`,
      component: require.resolve(`./src/templates/post/Post.js`),
      context: { slug: slug },
    });
  });
};
